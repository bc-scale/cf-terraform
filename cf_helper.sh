#!/usr/bin/env bash

CF_ACCOUNT_OPTIONS="--zone $TF_VAR_cloudflare_zone_id"
TIMEOUT=30
RESOURCES_FILE="$(pwd)/resources"
TARGET_DIR="$(pwd)/target"
# Добавляется поле zone_id
SED_ZONE_ID_INSERT='/^\s*zone_id/d; /^\s*resource/a\  \zone_id = var.cloudflare_zone_id'
# Исправляются ссылки на id фильтра.
SED_FILTER_ID_FIX='s/\(filter_id\s*=\s*\)\"\(.*\)\"/\1cloudflare_filter.terraform_managed_resource_\2.id/g'
# Если в правилах встречается упоминание домена с которого правила импортируются, то оно заменяется на домен в который правила будут залиты.
SED_RENAME_DOMAIN="/^\s*target/s/$CLOUDFLARE_ZONE/$CLOUDFLARE_ZONE_TO/g"

function countdown() {
    for ((i=$TIMEOUT;i>0;i--))
    do
        echo -ne "\rWait $i sec..."
        sleep 1
    done
    echo
}

function resource_name() {
    echo "$(echo $CF_IMPORT_OPTIONS | awk '{print $2}')"
}

function cmd_loop(){
    # Иногда API всё же даёт сбой на ровном месте, поэтому повторяем попытку с таймаутом
    local res
    res=""
    while ([[ $res == "" ]])
    do 
        res="$(cf-terraforming $1 $CF_ACCOUNT_OPTIONS $CF_IMPORT_OPTIONS)"
        if [[ $res == "" ]]; then
            countdown 1>&2
        fi
    done
    echo "$res"
}

function transform(){
    sed -i "$1" "$(pwd)/$2"
}

function generate_resources_files() {
    terraform init
    echo "Resources files creating..."
    cat $RESOURCES_FILE | while read line
    do
    echo $line | grep -Pv '^#' > /dev/null
    if [[ $? -eq 0 ]]; then
        CF_IMPORT_OPTIONS="--resource-type $line"
        echo "$(cmd_loop generate)" > $(resource_name).tf
        echo "Resource file for $(resource_name) generated"
    fi
    done
}

function import_resources_state() {
    if [[ $1 == "clean" ]];then
        rm -f *.tfstate
        rm -f *tfstate.backup
    fi
    cat $RESOURCES_FILE | while read resources_group
    do
        echo $resources_group | grep -Pv '^#' > /dev/null
        if [[ $? -eq 0 ]]; then
            CF_IMPORT_OPTIONS="--resource-type $resources_group"
            if [[ -f "$resources_group.tf" ]];then
                echo "$(cmd_loop import)" | while read line
                do
                eval $line;
                done
            else
                echo "Resources file $resources_group.tf not found. Generate it or remove resouce in $RESOURCES_FILE"
            fi
        fi
    done
}

function files_processing() {
    ls -1 cloudflare*.tf | while read line
    do
        if [[ $1 == "full" ]];then
            case $line in
            "cloudflare_firewall_rule.tf")
                transform "$SED_FILTER_ID_FIX" "$line"
            ;;
            "cloudflare_page_rule.tf")
                transform "$SED_RENAME_DOMAIN" "$line"
            ;;
            esac
        fi
        transform "$SED_ZONE_ID_INSERT" "$line"
    done
}

function get_zones_id() {
    res=$(terraform refresh)
    which jq > /dev/null
    if [[ $? ]];then
        terraform output -json | jq '.domain_zones.value.zones'
    else
        echo $res
    fi
}

function make_target(){
    rm -rf $TARGET_DIR
    mkdir $TARGET_DIR
    cp provider.tf $TARGET_DIR/
    cp -r .terraform $TARGET_DIR
    cd $TARGET_DIR
    CF_ACCOUNT_OPTIONS="--zone $CLOUDFLARE_ZONE_ID_TO"
    generate_resources_files
    files_processing
    echo "cloudflare_zone_id = \"$CLOUDFLARE_ZONE_ID_TO\"" > terraform.tfvars
    cd $TARGET_DIR
    import_resources_state
    terraform destroy
    rm -f *.tf
    rm -f *.tfstate
    rm -f *.tfstate.backup
    cp $TARGET_DIR/../*.tf $TARGET_DIR/
    files_processing full
    terraform apply -auto-approve
}

case $1 in
"generate")
    generate_resources_files
;;
"import")
    if [[ $2 == "clean" ]];then
        import_resources_state clean
    else
        import_resources_state
    fi
;;
"transform")
    if [[ $2 == "full" ]];then
        files_processing full
    else
        files_processing
    fi
;;
"get_zones_id")
    get_zones_id
;;
"make_target")
    make_target
;;
"prepare_source")
    generate_resources_files
    files_processing
    import_resources_state clean
    get_zones_id
;;
*)
    echo -e "Valid options:\ngenerate\nimport\nimport clean\ntransform\ntransform full\nget_zones_id\nmake_target\nprepare_source"
    exit 0;
;;
esac
